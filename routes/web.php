<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*    Frontend Section Routes            */
Route::get('/', 'FrontendController@index');
Route::get('/cart', 'FrontendController@cart');
Route::get('/singleproduct', 'FrontendController@singleproduct');
Route::get('/shoppage', 'FrontendController@shop');
Route::get('/checkout', 'FrontendController@checkout');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['prefix'=>'images'] ,function(){

Route::group(['prefix'=> 'slider'] ,function(){
  Route::get('/', 'SliderController@index');
  Route::get('add', 'SliderController@show');
  Route::post('store','SliderController@store');
  Route::get('{id}/delete','SliderController@delete');
});

Route::group(['prefix'=> 'latest-products'] ,function(){
  Route::get('/', 'LatestProductsController@index');
  Route::get('add', 'LatestProductsController@show');
  Route::post('store','LatestProductsController@store');
  Route::get('{id}/delete','LatestProductsController@delete');
});

Route::group(['prefix'=> 'brands'] ,function(){
  Route::get('/', 'BrandsController@index');
  Route::get('add', 'BrandsController@show');
  Route::post('store','BrandsController@store');
  Route::get('{id}/delete','BrandsController@delete');
});

Route::group(['prefix'=> 'top-sellers'] ,function(){
  Route::get('/', 'TopSellerController@index');
  Route::get('add', 'TopSellerController@show');
  Route::post('store','TopSellerController@store');
  Route::get('{id}/delete','TopSellerController@delete');
});

Route::group(['prefix'=> 'recently-view'] ,function(){
  Route::get('/', 'RecentlyViewController@index');
  Route::get('add', 'RecentlyViewController@show');
  Route::post('store','RecentlyViewController@store');
  Route::get('{id}/delete','RecentlyViewController@delete');
});

Route::group(['prefix'=> 'top-new'] ,function(){
  Route::get('/', 'TopNewController@index');
  Route::get('add', 'TopNewController@show');
  Route::post('store','TopNewController@store');
  Route::get('{id}/delete','TopNewController@delete');
});

});


Route::group(['prefix'=> 'user'] ,function(){
  Route::get('wallet', 'UserController@index');
});
