@extends('layouts.userlogin')
@section('content')
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth register-full-bg">
        <div class="row w-100">
          <div class="col-lg-4 mx-auto">
            <div class="auth-form-light text-left p-5">
              <h2>{{ __('Register') }}</h2>
              <h4 class="font-weight-light">Hello! let's get started</h4>
                <form method="POST" action="{{ route('register') }}" class="pt-4">
                    @csrf
                  <div class="form-group">
                    <label for="name">{{ __('Name') }}</label>
                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                    <i class="mdi mdi-account"></i>
                    @if ($errors->has('name'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="form-group">
                      <label for="email">{{ __('E-Mail Address') }}</label>
                      <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                      <i class="mdi mdi-eye"></i>
                      @if ($errors->has('email'))
                          <span class="invalid-feedback">
                              <strong>{{ $errors->first('email') }}</strong>
                          </span>
                     @endif
                  </div>
                  <div class="form-group">
                    <label for="password">{{ __('Password') }}</label>
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                      <i class="mdi mdi-eye"></i>
                    @if ($errors->has('password'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="form-group">
                    <label for="password-confirm">{{ __('Confirm Password') }}</label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                      <i class="mdi mdi-eye"></i>
                    @if ($errors->has('password_confirmation'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="mt-5">
                    <button type="submit" class="btn btn-block btn-primary btn-lg font-weight-medium" href="/register">{{ __('Register') }}</button>
                  </div>
                  <div class="mt-2 text-center">
                    <a href="/login" class="auth-link text-black">Already have an account? <span class="font-weight-medium">Sign in</span></a>
                  </div>
                  <div class="mt-2 text-center">
                    <a href="/" class="auth-link text-black"><u>Go To Home</u></a>
                  </div>
                </form>
            </div>
          </div>
        </div>
@endsection
