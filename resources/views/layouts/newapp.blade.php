<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Online Mobile Store</title>

    <link href="/adminpanel/css/bootstrap.min.css" rel="stylesheet">
    <link href="/adminpanel/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- FooTable -->
    <link href="/adminpanel/css/plugins/footable/footable.core.css" rel="stylesheet">

    <link href="/adminpanel/css/animate.css" rel="stylesheet">
    <link href="/adminpanel/css/style.css" rel="stylesheet">



</head>

<body>

    <div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        @include('backend.includes.leftnavbar')
    </nav>

        <div id="page-wrapper" class="gray-bg">
          <div class="row border-bottom">
          @include('backend.includes.topheader')
          </div>
        <div class="row wrapper border-bottom white-bg page-heading">
                @include('backend.includes.back_mainheading')
            </div>
        <div class="wrapper wrapper-content animated fadeInRight ecommerce">
            @yield('content')
        </div>
        @include('backend.includes.footer')
        </div>
        </div>
    <!-- Mainly scripts -->
    <script src="/adminpanel/js/jquery-3.1.1.min.js"></script>
    <script src="/adminpanel/js/bootstrap.min.js"></script>
    <script src="/adminpanel/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/adminpanel/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="/adminpanel/js/inspinia.js"></script>
    <script src="/adminpanel/js/plugins/pace/pace.min.js"></script>

    <!-- FooTable -->
    <script src="/adminpanel/js/plugins/footable/footable.all.min.js"></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function() {

            $('.footable').footable();

        });

    </script>

</body>

</html>
