<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Gourav Communication</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="/userlogin/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="/userlogin/css/simple-line-icons.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="/userlogin/css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="/userlogin/images/favicon.png" />
</head>

<body>
  <div class="container-scroller">
    @yield('content')
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="/js/jquery.min.js"></script>
  <script src="/userlogin/js/popper.min.js"></script>
  <script src="/adminpanel/js/bootstrap.min.js"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="/js/off-canvas.js"></script>
  <script src="/js/misc.js"></script>
  <!-- endinject -->
</body>

</html>
