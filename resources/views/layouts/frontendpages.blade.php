@include('frontend.includes.header')
  <body>
    <div class="header-area">
      @include('frontend.includes.header_area')
    </div> <!-- End header area -->

    <div class="site-branding-area">
        @include('frontend.includes.cart_logo')
    </div> <!-- End site branding area -->

    <div class="mainmenu-area">
        @include('frontend.includes.menu')
    </div> <!-- End mainmenu area -->


    <div class="product-big-title-area">
        @include('frontend.includes.product_title')
    </div> <!-- End Page title area -->

    <div class="maincontent-area">
        <div class="zigzag-bottom"></div>
         @yield('content')
    </div> <!-- End main content area -->


      @include('frontend.includes.footer')

    <!-- Latest jQuery form server -->
    <script src="https://code.jquery.com/jquery.min.js"></script>

    <!-- Bootstrap JS form CDN -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

    <!-- jQuery sticky menu -->
    <script src="/js/owl.carousel.min.js"></script>
    <script src="/js/jquery.sticky.js"></script>

    <!-- jQuery easing -->
    <script src="/js/jquery.easing.1.3.min.js"></script>

    <!-- Main Script -->
    <script src="/js/main.js"></script>

    <!-- Slider -->
    <script type="text/javascript" src="/js/bxslider.min.js"></script>
	<script type="text/javascript" src="/js/script.slider.js"></script>
  </body>
</html>
