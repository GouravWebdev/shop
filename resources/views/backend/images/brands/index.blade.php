@extends('layouts.newapp')
@section('content')
<div class="ibox-content m-b-sm border-bottom">
<div class="row">
  <div class="col-sm-4">
  </div>
  <div class="col-sm-2">
  </div>
  <div class="col-sm-2">
  </div>
  <div class="col-sm-2">
  </div>
  <div class="col-sm-2">
    <div class="form-group">
      <a href="/images/brands/add"><button type="button" class="btn-primary btnalign" value="Add Slider Images">Add Brands Images</button></a>
    </div>
  </div>
</div>
</div>
<div class="row">
<div class="col-lg-12">
  <div class="ibox">
    <div class="ibox-content">
      <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
        <thead>
          <tr>
            <th data-toggle="true">#</th>
            <th data-hide="phone">Title</th>
            <th data-hide="all">Path</th>
            <th  data-sort-ignore="true">Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach($brands as $brand)
          <tr>
            <td>
              {{$brand->id}}
            </td>
            <td>
              {{$brand->title}}
            </td>
            <td>
              {{$brand->path}}
            </td>
            <td>
              <div class="btn-group">
                <a href="/images/brands/{{$brand->id}}/delete"><button class="btn-white btn btn-xs">Delete</button></a>
              </div>
            </td>
          </tr>
          @endforeach
        </tbody>
        <tfoot>
          <tr>
            <td colspan="6">
              <ul class="pagination pull-right"></ul>
            </td>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>
</div>
@endsection
