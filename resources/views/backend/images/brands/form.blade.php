@extends('layouts.newapp')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Add Images Slider For Frontside Slider.</h5>
        <div class="ibox-tools">
          <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
          </a>
        </div>
      </div>
      <div class="ibox-content">
        <form method="post"  action="/images/brands/store" class="form-horizontal" enctype="multipart/form-data" >
          {{csrf_field()}}
          <div class="form-group"><label class="col-sm-2 control-label">Enter Tittle:</label>
            <div class="col-sm-10"><input type="text" name="tittle" class="form-control"></div>
          </div>
          <div class="hr-line-dashed"></div>
          <div class="form-group"><label class="col-sm-2 control-label">Upload File:</label>
            <div class="col-sm-10">
              <div class="input-group m-b">
                <input type="file" name="path" class="form-control"></div>
              </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
              <div class="col-sm-4 col-sm-offset-2">
                <a href="/images/brands"><button class="btn btn-white" type="button">Cancel</button></a>
                <button class="btn btn-primary" type="submit">Save</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
