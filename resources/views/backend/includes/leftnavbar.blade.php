@php
$segment1= Request::segment(1);
$segment2= Request::segment(2);
@endphp
<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> <span>
                        <img alt="image" class="img-circle" src="/adminpanel/img/profile_small.jpg" />
                         </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">David Williams</strong>
                        </span>
                        @if(Auth::check() && Auth::user()->roles_id == 2)
                        <span class="text-muted text-xs block">Mobile Owner<b class="caret"></b></span> </span> </a>
                        @else
                        <span class="text-muted text-xs block">User<b class="caret"></b></span> </span> </a>
                        @endif
                 </div>
                <div class="logo-element">
                    +MS
                </div>
            </li>
            @if(Auth::check() && Auth::user()->roles_id == 2)
            <li class="{{isset($segment1) && ($segment1=='home')?'active':''}}">
                <a href="/home"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span> <span class="fa arrow"></span></a>
            </li>
            <li class="{{isset($segment1) && ($segment1=='images')?'active':''}}">
                <a href="#"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Images</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li class="{{isset($segment2) && ($segment2=='slider')?'active':''}}"><a href="/images/slider">Slider</a></li>
                    <li class="{{isset($segment2) && ($segment2=='latest-products')?'active':''}}"><a href="/images/latest-products">Latest-Products</a></li>
                    <li class="{{isset($segment2) && ($segment2=='brands')?'active':''}}"><a href="/images/brands">Brands</a></li>
                    <li class="{{isset($segment2) && ($segment2=='top-sellers')?'active':''}}"><a href="/images/top-sellers">Top-Seller</a></li>
                    <li class="{{isset($segment2) && ($segment2=='recently-view')?'active':''}}"><a href="/images/recently-view">Recently-view</a></li>
                    <li class="{{isset($segment2) && ($segment2=='top-new')?'active':''}}"><a href="/images/top-new">Top-New</a></li>
                </ul>
            </li>
            @else
            <li class="active">
                <a href="/home"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span> <span class="fa arrow"></span></a>
            </li>
            <li>
                <a href="/user/wallet"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Wallet</span><span class="fa arrow"></span></a>
            </li>
            <li>
                <a href="/user/profile"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Profile</span><span class="fa arrow"></span></a>
            </li>
            <li>
                <a href="/user/history"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">History</span><span class="fa arrow"></span></a>
            </li>
            <li>
                <a href="/user/account-setting"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Account Setting</span><span class="fa arrow"></span></a>
            </li>
            <li>
                <a href="/user/account-deactivate"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Account Deactivate</span><span class="fa arrow"></span></a>
            </li>
            @endif
        </ul>

    </div>
</nav>
