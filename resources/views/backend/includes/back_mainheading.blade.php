@php
$segment1 = Request::segment(1);
$segment2 = Request::segment(2);
$segment3 = Request::segment(3);
@endphp
<div class="col-lg-10">
    <h2>Online Mobile Store</h2>
    <ol class="breadcrumb">
        <li>
            {{$segment1}}
        </li>
        <li class="active">
            {{$segment2}} {{isset($segment3)? '/'.''.$segment3:''}}
        </li>
    </ol>
</div>
<div class="col-lg-2"></div>
