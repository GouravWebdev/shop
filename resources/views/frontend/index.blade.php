@extends('layouts.frontend')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="latest-product">
                <h2 class="section-title">Latest Products</h2>
                <div class="product-carousel">
                  @foreach($products as $product)
                    <div class="single-product">
                        <div class="product-f-image">
                            <img src="/{{$product->path}}" alt="">
                            <div class="product-hover">
                                <a href="#" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                <a href="#" class="view-details-link"><i class="fa fa-link"></i> See details</a>
                            </div>
                        </div>
                        <h2><a href="#">{{$product->tittle}}</a></h2>
                        <div class="product-carousel-price">
                            <ins>Rs.{{$product->price}}</ins> <del>Rs.{{$product->old_price}}</del>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
