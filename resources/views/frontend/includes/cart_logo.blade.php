<div class="container">
    <div class="row">
        <div class="col-sm-6">
            <div class="logo">
                <h1><a href="/">Online <span>Mobile Store</span></a></h1>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="shopping-item">
                <a href="/shop/cart">Cart - <span class="cart-amunt">$100</span> <i class="fa fa-shopping-cart"></i> <span class="product-count">5</span></a>
            </div>
        </div>
    </div>
</div>
