<div class="block-slider block-slider4">
  <ul class="" id="bxslider-home4">
    @foreach($sliders as $slider)
    <li>
      <img src="/{{$slider->path}}" alt="Slide">
      <div class="caption-group">
        <h2 class="caption title">
          <span class="primary"><strong>{{$slider->tittle}}</strong></span>
        </h2>
        <a class="caption button-radius" href="/shoppage"><span class="icon"></span>Shop now</a>
      </div>
    </li>
    @endforeach
  </ul>
</div>
