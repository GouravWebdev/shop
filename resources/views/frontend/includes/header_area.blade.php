<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="user-menu">
                <ul>
                    <li><a href="/cart"><i class="fa fa-user"></i> My Cart</a></li>
                    <li><a href="/checkout"><i class="fa fa-user"></i> Checkout</a></li>
                    <li><a href="/register"><i class="fa fa-user"></i>Register</a></li>
                    <li><a href="/login"><i class="fa fa-user"></i> Login</a></li>
                </ul>
            </div>
        </div>

        <div class="col-md-4">
            <div class="header-right">
                <ul class="list-unstyled list-inline">
                    <li class="dropdown dropdown-small">
                        <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><span class="key">Currency :</span><span class="value">INR </span></a>
                    </li>

                    <li class="dropdown dropdown-small">
                        <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><span class="key">Language :</span><span class="value">English </span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
