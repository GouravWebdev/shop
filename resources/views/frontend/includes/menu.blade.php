@php
use App\Menu;
$menus = Menu::get();
@endphp
<div class="container">
    <div class="row">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="/ ">Home</a></li>
              @foreach($menus as $menu)
                <li><a href="/{{ $menu->name }} ">{{ $menu->name }}</a></li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
