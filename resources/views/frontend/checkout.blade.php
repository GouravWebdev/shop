@extends('layouts.frontendpages')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="single-sidebar">
                <!-- <h2 class="sidebar-title">Search Products</h2>
                <form action="">
                    <input type="text" placeholder="Search products...">
                    <input type="submit" value="Search">
                </form> -->
            </div>

            <div class="single-sidebar">
                <h2 class="sidebar-title">Products</h2>
                <div class="thubmnail-recent">
                    <img src="/img/product-thumb-1.jpg" class="recent-thumb" alt="">
                    <h2><a href="single-product.html">Sony Smart TV - 2015</a></h2>
                    <div class="product-sidebar-price">
                        <ins>$700.00</ins> <del>$100.00</del>
                    </div>
                </div>
                <div class="thubmnail-recent">
                    <img src="/img/product-thumb-1.jpg" class="recent-thumb" alt="">
                    <h2><a href="single-product.html">Sony Smart TV - 2015</a></h2>
                    <div class="product-sidebar-price">
                        <ins>$700.00</ins> <del>$100.00</del>
                    </div>
                </div>
                <div class="thubmnail-recent">
                    <img src="/img/product-thumb-1.jpg" class="recent-thumb" alt="">
                    <h2><a href="single-product.html">Sony Smart TV - 2015</a></h2>
                    <div class="product-sidebar-price">
                        <ins>$700.00</ins> <del>$100.00</del>
                    </div>
                </div>
                <div class="thubmnail-recent">
                    <img src="/img/product-thumb-1.jpg" class="recent-thumb" alt="">
                    <h2><a href="single-product.html">Sony Smart TV - 2015</a></h2>
                    <div class="product-sidebar-price">
                        <ins>$700.00</ins> <del>$100.00</del>
                    </div>
                </div>
            </div>

            <div class="single-sidebar">
                <h2 class="sidebar-title">Recent Posts</h2>
                <ul>
                    <li><a href="single-product.html">Sony Smart TV - 2015</a></li>
                    <li><a href="single-product.html">Sony Smart TV - 2015</a></li>
                    <li><a href="single-product.html">Sony Smart TV - 2015</a></li>
                    <li><a href="single-product.html">Sony Smart TV - 2015</a></li>
                    <li><a href="single-product.html">Sony Smart TV - 2015</a></li>
                </ul>
            </div>
        </div>

        <div class="col-md-8">
            <div class="product-content-right">
                <div class="woocommerce">
                    <div class="woocommerce-info">Returning customer? <a class="showlogin" data-toggle="collapse" href="#login-form-wrap" aria-expanded="false" aria-controls="login-form-wrap">Click here to login</a>
                    </div>
                    <form id="login-form-wrap" class="login collapse" method="post">
                        <p>If you have shopped with us before, please enter your details in the boxes below. If you are a new customer please proceed to the Billing &amp; Shipping section.</p>
                        <p class="form-row form-row-first">
                            <label for="username">Username or email <span class="required">*</span>
                            </label>
                            <input type="text" id="username" name="username" class="input-text">
                        </p>
                        <p class="form-row form-row-last">
                            <label for="password">Password <span class="required">*</span>
                            </label>
                            <input type="password" id="password" name="password" class="input-text">
                        </p>
                        <div class="clear"></div>
                        <p class="form-row">
                            <input type="submit" value="Login" name="login" class="button">
                            <label class="inline" for="rememberme"><input type="checkbox" value="forever" id="rememberme" name="rememberme"> Remember me </label>
                        </p>
                        <p class="lost_password">
                            <a href="#">Lost your password?</a>
                        </p>
                        <div class="clear"></div>
                    </form>
                    <form enctype="multipart/form-data" action="https://www.paypal.com/" class="checkout" method="post" name="checkout">
                        <div id="customer_details" class="col2-set">
                            <div class="col-1">  </div>
                            <div class="col-2">
                                <div class="woocommerce-shipping-fields">
                                    <h3 id="ship-to-different-address">
                                    <div class="shipping_address" style="display: block;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h3 id="order_review_heading">Your order</h3>

                        <div id="order_review" style="position: relative;">
                            <table class="shop_table">
                                <thead>
                                    <tr>
                                        <th class="product-name">Product</th>
                                        <th class="product-total">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="cart_item">
                                        <td class="product-name">
                                            Ship Your Idea <strong class="product-quantity">× 1</strong> </td>
                                        <td class="product-total">
                                            <span class="amount">£15.00</span> </td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr class="cart-subtotal">
                                        <th>Cart Subtotal</th>
                                        <td><span class="amount">£15.00</span>
                                        </td>
                                    </tr>
                                    <tr class="shipping">
                                        <th>Shipping and Handling</th>
                                        <td>
                                            Free Shipping
                                            <input type="hidden" class="shipping_method" value="free_shipping" id="shipping_method_0" data-index="0" name="shipping_method[0]">
                                        </td>
                                    </tr>
                                    <tr class="order-total">
                                        <th>Order Total</th>
                                        <td><strong><span class="amount">£15.00</span></strong> </td>
                                    </tr>
                                </tfoot>
                            </table>
                            <div id="payment">
                                <ul class="payment_methods methods">
                                    <li class="payment_method_paypal">
                                        <input type="radio" data-order_button_text="Proceed to PayPal" value="paypal" name="payment_method" class="input-radio" id="payment_method_paypal">
                                        <label for="payment_method_paypal">PayPal <img alt="PayPal Acceptance Mark" src="https://www.paypalobjects.com/webstatic/mktg/Logo/AM_mc_vs_ms_ae_UK.png"><a title="What is PayPal?" onclick="javascript:window.open('https://www.paypal.com/gb/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;" class="about_paypal" href="https://www.paypal.com/">What is PayPal?</a>
                                        </label>
                                        <div style="display:none;" class="payment_box payment_method_paypal">
                                            <p>Pay via PayPal; you can pay with your credit card if you don’t have a PayPal account.</p>
                                        </div>
                                    </li>
                                </ul>
                                <div class="form-row place-order">
                                    <input type="submit" data-value="Place order" value="Place order" id="place_order" name="woocommerce_checkout_place_order" class="button alt">
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
