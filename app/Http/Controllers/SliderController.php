<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Slider;
class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['sliders'] = Slider::get();
        return view('backend.images.slider.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
        'tittle' => 'required',
        'path' => 'required|mimes:jpg,png,jpeg'
      ]);
        $slider = Slider::firstorNew(['id'=>$request->id]);
        $slider->tittle = $request->tittle;
        if ($request->hasFile('path'))
        {
            $file=$request->file('path');
            $path = 'uploads/images/sliders/'; //We are not defined "/" here because it is represent root directory.
            $filename = $file->getClientOriginalName();
            $file->move($path, $filename);
            $slider->path = ($path.$filename);
          }
        $slider->save();
        return redirect('/images/slider');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('backend.images.slider.form');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        Slider::where('id',$id)->delete();
        return back();
    }
}
