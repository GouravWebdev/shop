<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LatestProducts;
class LatestProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data['products'] = LatestProducts::get();
      return view('backend.images.latest_products.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate(['path'=>'required']);
      $products = LatestProducts::firstorNew(['id'=>$request->id]);
      $products->tittle = $request->tittle;
      $products->price = $request->price;
      $products->old_price = $request->old_price;
      if ($request->hasFile('path'))
      {
          $file=$request->file('path');
          $path = 'uploads/images/products/'; //We are not defined "/" here because it is represent root directory.
          $filename = $file->getClientOriginalName();
          $file->move($path, $filename);
            $products->path = ($path.$filename);
        }
          $products->save();
          return redirect('images/latest-products');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
      return view('backend.images.latest_products.form');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
      LatestProducts::where('id',$id)->delete();
      return back();  
    }
}
