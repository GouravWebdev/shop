<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RecentlyView;
class RecentlyViewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data['recentlies'] = RecentlyView::get();
      return view('backend.images.recently_view.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate(['path'=>'required']);
      $recently = RecentlyView::firstorNew(['id'=>$request->id]);
      $recently->title = $request->tittle;
      $recently->price = $request->price;
      $recently->old_price = $request->old_price;
      if ($request->hasFile('path'))
      {
          $file=$request->file('path');
          $path = 'uploads/images/recently/'; //We are not defined "/" here because it is represent root directory.
          $filename = $file->getClientOriginalName();
          $file->move($path, $filename);
            $recently->path = ($path.$filename);
        }
          $recently->save();
          return redirect('images/recently-view');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
          return view('backend.images.recently_view.form');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
      RecentlyView::where('id',$id)->delete();
      return back();
    }
}
