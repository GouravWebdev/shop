<?php

namespace App\Http\Controllers;
use App\ServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if(Auth::check() && Auth::user()->roles_id==2)
        return view('home')->with('success', 'You are successfully logged in');
        else
        return view('backend.user.home')->with('success', 'You are successfully logged in');
    }

    public function store(Request $request)
    {

    }

    public function show()
    {

    }
}
