<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TopNew;

class TopNewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data['topnews'] = TopNew::get();
        return view('backend.images.top_new.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
        'tittle' => 'required',
        'path' => 'required|mimes:jpg,png,jpeg'
      ]);
        $slider = TopNew::firstorNew(['id'=>$request->id]);
        $slider->title = $request->tittle;
        $slider->price = $request->price;
        $slider->old_price = $request->old_price;
        if ($request->hasFile('path'))
        {
            $file=$request->file('path');
            $path = 'uploads/images/topnew/'; //We are not defined "/" here because it is represent root directory.
            $filename = $file->getClientOriginalName();
            $file->move($path, $filename);
            $slider->path = ($path.$filename);
          }
        $slider->save();
        return redirect('/images/top-new');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
      return view('backend.images.top_new.form');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
      TopNew::where('id',$id)->delete();
      return back();
    }
}
