<?php

namespace App\Http\Controllers;
use App\Menu;
use Illuminate\Http\Request;
use App\Slider;
use App\LatestProducts;


class FrontendController extends Controller
{
    public function index()
    {
      $data['sliders'] = Slider::get();
      $data['products'] = LatestProducts::get();
       return view('frontend.index',$data);
     }

     public function cart()
     {
       return view('frontend.cart');
     }

     public function singleproduct()
     {
       return view('frontend.single_product');
     }


    public function shop()
    {
      return view('frontend.shop');
    }

    public function checkout()
    {
    return view('frontend.checkout');
    }
}
