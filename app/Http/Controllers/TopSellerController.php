<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TopSellers;
class TopSellerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data['sellers'] = TopSellers::get();
        return view('backend.images.top_seller.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate(['path'=>'required']);
      $brands = TopSellers::firstorNew(['id'=>$request->id]);
      $brands->title = $request->tittle;
      $brands->price = $request->price;
      $brands->old_price = $request->old_price;
      if ($request->hasFile('path'))
      {
          $file=$request->file('path');
          $path = 'uploads/images/topseller/'; //We are not defined "/" here because it is represent root directory.
          $filename = $file->getClientOriginalName();
          $file->move($path, $filename);
            $brands->path = ($path.$filename);
        }
          $brands->save();
          return redirect('images/top-sellers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('backend.images.top_seller.form');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
      TopSellers::where('id',$id)->delete();
      return back();
    }
}
