<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPriceAndOldPriceToTopSellersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('top_sellers', function (Blueprint $table) {
          $table->string('price')->after('title');
          $table->string('old_price')->after('price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('top_sellers', function (Blueprint $table) {
          $table->dropColumn('price');
          $table->dropColumn('old_price');
        });
    }
}
