<?php

use Illuminate\Database\Seeder;
use App\Menu;
class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menus  = [
          ['name'=>'home'],
          ['name'=> 'shop page'],
          ['name'=>'single Product'],
          ['name'=>'cart'],
          ['name'=>'checkout'],
          ['name'=>'category'],
          ['name'=>'others'],
          ['name'=>'contact']
        ];


      foreach($menus as $menu)
      {

      $menu = Menu::firstorNew(
                                [
                                   'name'=>$menu['name'],
                                ]
                              );
      $menu->save();
      }
    }
}
